import React, { Component } from 'react'

import './App.scss'

import CartList from '../components/cartList/cartList'
import CardDetail from '../components/cardDetail/cardDetail'

class App extends Component {
  render() {
    return (
      <div className="main-container">
        <CartList />
        <CardDetail />
      </div>
    )
  }
}

export default App