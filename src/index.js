import React from 'react'
import ReactDOM from 'react-dom'

import 'normalize.css'

import App from './container/App'

import { Provider } from 'react-redux'
import store from './stores/createStore'

class Index extends React.Component {
  render() {
    return (
      <App />
    )
  }
}

ReactDOM.render(<Provider store={store}><Index /></Provider>, document.querySelector('#app'))