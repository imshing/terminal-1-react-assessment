import React, { Component } from 'react'
import Cards from 'react-credit-cards'
import 'react-credit-cards/es/styles-compiled.css'
import './cardDetail.scss'
import {
  formatCreditCardNumber,
  formatCVC,
  formatExpirationDate,
  formatFormData,
} from '../../util'

class CardDetail extends Component {
  state = {
    number: '',
    name: '',
    expiry: '',
    cvc: ''
  }

  handleInputChange = ({ target }) => {
    if (target.name === 'number') {
      target.value = formatCreditCardNumber(target.value);
    } else if (target.name === 'expiry') {
      target.value = formatExpirationDate(target.value);
    } else if (target.name === 'cvc') {
      target.value = formatCVC(target.value);
    }
    this.setState({ [target.name]: target.value });
  };

  render() {
    return (
      <div className="card-detail">
        <div className="card-detail__container">
          <div className="card-detail__title">Card Details</div>
          <div className="card-detail__row">
            <div className="card-detail__label">Card Type</div>
            <Cards 
              number={this.state.number}
              name={this.state.name}
              expiry={this.state.expiry}
              cvc={this.state.cvc}
            />
          </div>
          <div className="card-detail__row">
            <div className="card-detail__label">Name on Card</div>
            <div className="card-detail__input">
              <input
                type="text"
                name="name"
                placeholder="Name"
                required
                onChange={this.handleInputChange}
              />
            </div>
          </div>
          <div className="card-detail__row">
            <div className="card-detail__label">Card Number</div>
            <div className="card-detail__input">
              <input
                type="tel"
                name="number" 
                placeholder="Card Number"
                pattern="[\d| ]{16,22}"
                required
                onChange={this.handleInputChange}
              />
            </div>
          </div>
          <div className="card-detail__row card-detail__row--2col">
            <div className="card-detail__col">
              <div className="card-detail__label">Expiration Date</div>
              <div className="card-detail__input">
                <input
                  type="tel"
                  name="expiry"
                  placeholder="MM/YY"
                  pattern="\d\d/\d\d"
                  required
                  onChange={this.handleInputChange}
                />
              </div>
            </div>
            <div className="card-detail__col">
              <div className="card-detail__label">CVC</div>
              <div className="card-detail__input">
                <input
                  type="tel"
                  name="cvc"
                  placeholder="CVC"
                  pattern="\d{3,4}"
                  required
                  onChange={this.handleInputChange}
                />
              </div>
            </div>
          </div>
          <div className="card-detail__row">
            <div className="card-detail__button">
              <button type="submit">Check Out</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default CardDetail
